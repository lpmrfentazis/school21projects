from task13 import mean_age

json_string = """
            [
                {
                    "name": "Петр",
                    "surname": "Петров",
                    "patronymic": "Васильевич",
                    "age": 23,
                    "occupation": "ойтишнек"
                },
                {
                    "name": "Василий",
                    "surname": "Васильев",
                    "patronymic": "Петрович",
                    "age": 24,
                    "occupation": "дворник"
                }
            ]
            """

print(mean_age(json_string=json_string))