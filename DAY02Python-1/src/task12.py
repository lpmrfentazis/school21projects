from collections import Counter

def get_popular_name_from_file(filename: str) -> list:
    with open(filename, 'r') as file:
        # get name from str
        names = [i.strip().split()[0] for i in file.readlines()]

        mostCommons = Counter(names).most_common()
        
        # if file is enpty
        if len(mostCommons) == 0:
            return []
        
        maxCount = mostCommons[0][1]

        return [i for i in mostCommons if i[1] == maxCount]