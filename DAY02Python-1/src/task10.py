def lists_sum(*lists, unique: bool = False) -> int:
    numbers = []

    for i in lists:
        numbers += i

    if unique:
        numbers = set(numbers)

    return sum(numbers)