from task11 import get_balance,  count_debts

transactions = [
            {"name": "Василий", "amount": 500},
            {"name": "Петя", "amount": 100},
            {"name": "Василий", "amount": -300},
        ]


print(get_balance("Василий", transactions=transactions))
print(count_debts(["Василий", "Петя", "Вова"], 150, transactions))
