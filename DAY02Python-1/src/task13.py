import json


def mean_age(json_string: str) -> str:
    data = json.loads(json_string)

    ages = [i["age"] for i in data]

    if len(ages) == 0:
        return 0
        
    # str(dict) use ' in keys, that's why we use json.dumps
    return json.dumps({"mean_age": sum(ages)/len(ages)})