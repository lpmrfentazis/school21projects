from datetime import datetime, timedelta

mes = input()

date = datetime.strptime(mes, "%d-%m-%Y")
date = date - timedelta(days=date.weekday())

print(date.strftime("%d-%m-%Y"))