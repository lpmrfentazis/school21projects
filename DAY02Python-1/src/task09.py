import datetime


def gift_count(month: int, budget: int, birthdays: dict) -> None:
    birthdaysCount = 0
    birthdayPeople = {}

    for man, date in birthdays.items():
        if date.month == month:
            birthdaysCount += 1
            birthdayPeople[man] = date.strftime("(%d.%m.%Y)")

    if birthdaysCount > 0:
        print(f"Именинники в месяце {birthdaysCount}: ", end='')
        print(*[ f'{man} {date}' for man, date in birthdayPeople.items()], sep=", ", end=". ")
        print(f"При бюджете {budget} они получат по {budget//birthdaysCount} рублей.")
    
    else:
        print("В этом месяце нет именинников.")
