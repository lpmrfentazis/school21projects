from task10 import lists_sum

print(lists_sum([1, 1], [1], [1, 2, 3]))
print(lists_sum([1, 1, 1], [1, 1], unique=True))
print(lists_sum([1, 1, 1], unique=False))