def get_balance(name: str, transactions: list) -> int:
    if name not in [i["name"] for i in transactions]:
        return 0

    return sum( [i["amount"] for i in transactions if i["name"] == name] )

def count_debts(names: list, amount: int, transactions: list) -> dict:
    result = {}
    
    for i in names:
        transactions.append({"name": i, "amount": -amount})
    
    for name in names:
        balance = get_balance(name, transactions)
        if balance > 0:
            result[name] = 0
        else:
            result[name] = -balance

    return result