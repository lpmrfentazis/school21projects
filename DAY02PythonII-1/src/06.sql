/*
    Мы хотим узнать, в какие магазины пользователи ходят чаще всего. 
    Для каждого уникального customer_id выберите id и адрес 
    самого популярного магазина. 
    Если таких магазинов несколько, выведите все. 
    Отсортируйте выборку по customer_id, store_id.
*/
WITH 
    users_stores AS (SELECT customer_id, store_id, COUNT(sale_id) 
                     FROM sales 
                     GROUP BY customer_id, store_id), 
    maxs AS (SELECT customer_id, MAX(count) 
             FROM users_stores GROUP BY customer_id)

SELECT users_stores.customer_id, 
       users_stores.store_id, 
       stores.address 
       
FROM users_stores 
JOIN stores ON users_stores.store_id = stores.store_id 
JOIN maxs ON users_stores.customer_id = maxs.customer_id 
                AND users_stores.count = maxs.max;