/*
    Найдите самый популярный продукт для каждого региона 
    и самый популярный регион для каждого продукта. 
    Добавьте колонку popularity_type с указанием типа 
    популярности со значениями 
    'самый популярный продукт для региона' 
    и 'самый популярный регион для продукта' 
    и выведите результаты в виде одной таблицы, 
    соединенной вертикально. 
    Сначала выведите самый популярный продукт 
    для региона по возрастанию региона, 
    затем самый популярный регион для продукта.
*/

WITH sales_regions AS (SELECT sale_id, product_id, region 
                            FROM sales 
                            JOIN stores ON sales.store_id = stores.store_id),
         counts_regions AS (SELECT product_id, region, COUNT(sale_id) 
                            FROM sales_regions 
                            GROUP BY product_id, region), 
         most_common AS (SELECT product_id, region, count
                         FROM counts_regions as t1
                         WHERE count = (SELECT MAX(count) 
                                        FROM counts_regions as t2 
                                        WHERE t2.product_id = t1.product_id 
                                                AND
                                            t2.region = t1.region))
    SELECT * FROM 
    (SELECT 'самый популярный продукт для региона' as popularity_type, t11.product_id, t11.region 
    FROM most_common AS t11 
    JOIN 
        (SELECT region, MAX(count) 
        FROM most_common 
        GROUP BY region) AS max_region_sales 
    ON t11.region = max_region_sales.region
    WHERE count = max) AS _
    UNION
    (SELECT 'самый популярный регион для продукта' as popularity_type, t1.product_id, region 
    FROM most_common AS t1 
    JOIN 
        (SELECT product_id, MAX(count) 
        FROM most_common 
        GROUP BY product_id) AS max_product_sales 
    ON t1.product_id = max_product_sales.product_id
    WHERE count = max)
    ORDER BY popularity_type DESC, region;