/*
    Мы хотим оценить, как меняется спрос на сосиски под новый год.
    Выведите общее количество продаж всех сосисок подневно за декабрь 
    (весовые и упаковочные считаем отдельно). 
    Данные по разным годам нужно усреднить 
    (т.е. допустим, что 19 декабря 2019 года продалось 50 весовых сосисок, 
    а 19 декабря 2020 года продалось 70 весовых сосисок. Для 19 декабря вывести нужно 60). 
    Формат вывода: дата (DD-MM), категория, единица измерения, количество. Если в один год были продажи, 
    а в другой нет, то среднее всё равно делим на 2.
*/

WITH desember AS (SELECT to_char(sale_date, 'DD-MM') as day, product_id, COUNT(sale_id), EXTRACT(YEAR FROM sale_date) as year
            FROM sales
            WHERE EXTRACT(MONTH FROM sale_date) = 12 GROUP BY product_id, day, year),
        types AS (SELECT day, year, category, name, count, CASE 
                        WHEN name LIKE '%уп.' 
                            THEN 'уп' 
                        ELSE 'вес' 
                    END as type 
                FROM desember
                JOIN products ON desember.product_id = products.product_id
                WHERE category='сосиски')
    
    SELECT day, category, type, CASE
                                    WHEN COUNT(*) = 1 
                                        THEN  AVG(count)::int / 2
                                    ELSE AVG(count)::int 
                                   END 
                                AS count 
    FROM types GROUP BY day, type, category;

