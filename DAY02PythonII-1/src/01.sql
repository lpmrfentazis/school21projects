/*
    В нашей сети цена товара назначается либо на две недели, 
    либо на одну. 
    Мы хотим проанализировать спрос на товары в те недели, 
    когда клиенты думают, что цены начали меняться чаще. 
    Выведите product_id, start_date, end_date тех цен, 
    которые назначались сразу после однонедельных цен 
    (например, если цена на товар назначалась 01.02.2020, 
    15.02.2020, 22.02.2020, выберите start_date 22.02.2020). 
    Отсортируйте выборку по product_id и start_date в порядке возрастания. 
    Гарантируется, что у периодов действия цен нет 
    пересекающихся дат в рамках одного продукта.
*/
WITH weeks as (SELECT product_id, start_date, end_date 
                    FROM prices WHERE end_date-start_date = 6) 
SELECT * FROM prices 
JOIN weeks ON prices.start_date = weeks.end_date 
              AND prices.product_id = weeks.product_id;