/*
    Выведите  product_id и минимальную и максимальную цену каждого продукта, 
    который был куплен в магазинах со store_id меньше 10.
*/
SELECT DISTINCT products.name, t.product_id, t.min, t.max 
    FROM (SELECT prices.product_id, MIN(price) as min, MAX(price) as max 
            FROM prices
            GROUP BY product_id) as t
    JOIN products ON t.product_id = products.product_id
    JOIN sales ON sales.product_id = t.product_id 
            WHERE sales.store_id < 10;