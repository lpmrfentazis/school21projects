/*
    Выведите: 
    индентификатор товара, 
    отчетную дату, 
    идентификатор магазина, 
    кол-во купленного товара, 
    кол-во на складе, 
    кол-во остатков 
    для магазина с индентификатором 33. 
    
    Сделайте итоговый запрос с использованием двух таблиц-представлений:
        - Первая CTE с названием sup_cnt. 
        При обращении к ней должна выдаваться 
        информация от товарах на складе: 
            product_id, 
            store_id, 
            и "кол-во на складе". 
        "кол-во на складе" - это количество товаров, 
            находящихся на складе к 01.01.2020. 
            Например, оно равняется 60 для product_id = 4.

        - Вторая CTE с названием s_cnt_, 
        в которой содержатся столбцы с названиями: 
            индентификатор товара, 
            отчетная дата, 
            идентификатор магазина, 
            кол-во купленного товара. 
        Для формирования s_cnt_ воспользуйтесь ранее созданной View. 
        Просьба учесть, 
        что если 01.01 товар купили 1 раз, 
        а 02.01 этот товар купили 2 раза, 
        то на отчетную дату 02.01 кол-во купленного товара = 3 
        (т.н. кумулятивная сумма). 

    Отчетная дата в запросе не должна превышать 20.01.2021. 
    Если информации о товаре на складе нет, 
    выведите -1, иначе количество товаров на складе. 
    Выведите данные по количеству на складе в порядке убывания.
*/

CREATE OR REPLACE VIEW s_cnt AS
WITH sales_date AS (
    SELECT product_id, store_id, COUNT(*) AS count_purchased
    FROM sales
    WHERE sale_date = '2020-01-01'
    GROUP BY product_id, store_id)

SELECT product_id, 
        '2020-01-01'::date AS reporting_date, 
        store_id, 
        count_purchased
        
FROM sales_date;


WITH sup_cnt AS (
    SELECT product_id, store_id, SUM(product_count) AS quantity_stock
    FROM supply
    WHERE store_id = 33 AND delivery_date <= '2020-01-01'
    GROUP BY product_id, store_id),

    s_cnt_ AS (
        SELECT product_id, reporting_date, store_id, SUM(count_purchased) OVER (PARTITION BY product_id, store_id ORDER BY reporting_date) AS cumulative_count_purchased
        FROM s_cnt
        WHERE reporting_date <= '2021-01-20')

SELECT s_cnt_.product_id,
       s_cnt_.reporting_date,
       s_cnt_.store_id,
       s_cnt_.cumulative_count_purchased,
       COALESCE(sup_cn, -1) A,
       COALESCE(sup_cn, 0) - s_cnt_.cumulative_count_purchased AS quantity_left_over

FROM s_cnt_
LEFT JOIN sup_cnt ON s_cnt_.product_id = sup_cnt.product_id 
                    AND s_cnt_.store_id = sup_cnt.store_id

WHERE s_cnt_.store_id = 33
ORDER B DESC;