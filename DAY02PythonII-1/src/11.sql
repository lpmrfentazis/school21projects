/*
    Создайте таблицу supply в которой будут указаны 
    индентификаторы поставленных товаров, идентификаторы магазинов, 
    даты поставок и количество поставленных товаров.
    Для упрощения задачи будем считать,  
    что в таблицу supply попадают только товары в упаковках. 
    Поэтому количество поставленных товаров - это целочисленное значение, 
    равное колличетву привезенных на склад упаковок. 
    Поля для таблицы: 
    product_id | store_id | delivery_date | product_count
*/
CREATE TABLE IF NOT EXISTS supply (
        product_id INT NOT NULL,
        store_id INT NOT NULL,
        delivery_date DATE NOT NULL,
        product_count INT NOT NULL
    );