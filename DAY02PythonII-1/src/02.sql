/*
    Выведите product_id и минимальную и максимальную цену 
    каждого продукта в алфавитном порядке названия продукта.
*/

SELECT products.name, t.product_id, t.min, t.max 
    FROM (SELECT prices.product_id, MIN(price) as min, MAX(price) as max 
            FROM prices GROUP BY product_id) as t
    JOIN products ON t.product_id = products.product_id
    ORDER BY products.name COLLATE "POSIX";