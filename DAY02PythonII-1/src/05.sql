/*
    Выведите самый популярный product_id среди незарегистрированных клиентов. 
    Если таких несколько, то выведите все.
*/
WITH maxs AS (SELECT product_id, COUNT(sale_id) as count 
    FROM sales WHERE customer_id IS NULL 
    GROUP BY product_id 
    ORDER BY count DESC) 

SELECT * FROM maxs WHERE count = (SELECT count FROM maxs LIMIT 1);