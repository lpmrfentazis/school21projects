/*
    Выполните задание 13 с использованием View с названием s_cnt, 
    в которой содержатся столбцы с названиями: индентификатор товара,
    отчетная дата, идентификатор магазина, 
    кол-во купленного товара за один день. 
    Создание View и запрос к ней можно описать в одном файле 
    через точку с запятой.
*/

CREATE OR REPLACE VIEW s_cnt AS
WITH sales_date AS (
    SELECT product_id, store_id, COUNT(*) AS count_purchased
    FROM sales
    WHERE sale_date = '2020-01-01'
    GROUP BY product_id, store_id
)

SELECT product_id, '2020-01-01'::date AS reporting_date, store_id, count_purchased
FROM sales_date;

WITH stock_end AS (
    SELECT s.product_id, s.store_id, SUM(s.product_count) - COALESCE(sd.count_purchased, 0) AS count_stock
    FROM supply s
    LEFT JOIN s_cnt sd ON s.product_id = sd.product_id AND s.store_id = sd.store_id
    WHERE s.delivery_date <= '2020-01-01'
    GROUP BY s.product_id, s.store_id, sd.count_purchased
)

SELECT s.product_id as индефикатор_товара, 
    '2020-01-01' отчетная_дата, 
    s.store_id as идентификатор_магазина, 
    sd.count_purchased as кол_во_купленного_товара, 
    s.count_stock as кол_во_остатков
    
FROM stock_end s
LEFT JOIN s_cnt sd USING (product_id, store_id)
ORDER BY s.product_id, s.store_id;