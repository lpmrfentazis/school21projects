/*
    Выведите название самого популярного товара в каждой категории на 
    23 февраля и 8 марта (по каждой дате отдельно).
*/
-- 8 Марта 
WITH m8 as ( SELECT name, category, sales.product_id, count 
             FROM (SELECT sales.product_id, COUNT(sales.sale_id) as count 
                    FROM (SELECT * FROM sales
                        WHERE (EXTRACT(MONTH FROM sale_date) = 3 
                        AND EXTRACT(DAY FROM sale_date) = 8)) AS sales 
                        GROUP BY product_id)
    as sales
    JOIN products ON sales.product_id = products.product_id) 

SELECT m8.category, name 
FROM m8 
JOIN (SELECT category,  MAX(count) as max 
    FROM m8 GROUP BY category) as t 
        ON m8.category = t.category AND m8.count = t.max;

-- 23 февраля 

WITH f23 as ( SELECT name, category, sales.product_id, count FROM
        (SELECT sales.product_id, COUNT(sales.sale_id) as count FROM (SELECT * FROM sales
    WHERE (EXTRACT(MONTH FROM sale_date) = 2 AND EXTRACT(DAY FROM sale_date) = 23)) as sales GROUP BY product_id)
    as sales
    JOIN products ON sales.product_id = products.product_id) 

SELECT f23.category, name 
FROM f23 
JOIN (SELECT category,  MAX(count) as max 
    FROM f23 GROUP BY category) as t 
        ON f23.category = t.category AND f23.count = t.max;