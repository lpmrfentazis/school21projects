/*
    Выведите по 3 самых популярных товара 
    у зарегистрированных и незарегистрированных пользователей. 
    Если таких товаров несколько, выведите все. С
    начала выведите номер, затем id продукта и тип пользователя. 
    Назовите столбец с указанием типа пользователя `customer_type`. 
    В этом задании нужно обойтись без CTE.
*/
SELECT ROW_NUMBER() OVER(ORDER BY count DESC) AS number, * FROM
(SELECT * 
    FROM (SELECT product_id, COUNT(sale_id), 'unregistered' AS customer_type
            FROM sales
            WHERE customer_id IS NULL
            GROUP BY product_id
            HAVING COUNT(sale_id) IN (SELECT DISTINCT COUNT(s.sale_id) as count1
                                        FROM sales AS s WHERE s.customer_id IS NULL
                                            GROUP BY s.product_id
                                                ORDER BY count1 DESC LIMIT 3)) AS t1
        UNION
        (SELECT product_id, COUNT(sale_id), 'registered' AS customer_type
            FROM sales
            WHERE customer_id IS NOT NULL
            GROUP BY product_id
            HAVING COUNT(sale_id) IN (SELECT DISTINCT COUNT(s.sale_id) as count1
                                        FROM sales AS s WHERE s.customer_id IS NOT NULL
                                            GROUP BY s.product_id
                                                ORDER BY count1 DESC LIMIT 3))) AS fuckingShit;