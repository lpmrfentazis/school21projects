/*
    Выведите количество купленного товара за 01.01.2020
     и количество остатков на складе на конец дня 
     (имеющееся количество продуктов на конец дня). 
     Количество остатков на складе на конец дня - 
     это разница между количеством товаров на складе и купленными 
     за день товарами. Сделайте выборку по данным которые, 
     хранятся в таблице supply. 
     Используйте такие названия столбцов: индентификатор товара, 
     отчетная дата, идентификатор магазина, 
     кол-во купленного товара, кол-во остатков.
*/
WITH sales_date AS (
    SELECT product_id, store_id, COUNT(*) AS count_purchased
    FROM sales
    WHERE sale_date = '2020-01-01'
    GROUP BY product_id, store_id
),

stock_end AS (
    SELECT s.product_id, s.store_id, SUM(s.product_count) - COALESCE(sd.count_purchased, 0) AS count_stock
    FROM supply s
    LEFT JOIN sales_date sd ON s.product_id = sd.product_id AND s.store_id = sd.store_id
    WHERE s.delivery_date <= '2020-01-01'
    GROUP BY s.product_id, s.store_id, sd.count_purchased
)

SELECT s.product_id as индефикатор_товара, 
    '2020-01-01' отчетная_дата, 
    s.store_id as идентификатор_магазина, 
    sd.count_purchased as кол_во_купленного_товара, 
    s.count_stock as кол_во_остатков
    
FROM stock_end s
LEFT JOIN sales_date sd USING (product_id, store_id)
ORDER BY s.product_id, s.store_id;