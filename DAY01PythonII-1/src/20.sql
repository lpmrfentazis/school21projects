/*
    Выведите количество продаж по месяцам в 2020 году. 
    Формат вывода: 
    двузначный номер месяца - количество продаж.
*/
SELECT DISTINCT to_char(EXTRACT(MONTH FROM sale_date)::int, '00') as month, COUNT(sale_id) FROM sales WHERE EXTRACT(YEAR FROM sale_date) = 2020 GROUP BY EXTRACT(MONTH FROM sale_date);