/*
    Выберите все уникальные сочетания
    имя-фамилия зарегистрированных клиентов.
*/
SELECT DISTINCT (name, surname) FROM customers;