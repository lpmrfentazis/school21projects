/*
   Выведите все уникальные бренды товаров в алфавитном порядке. 
*/
SELECT DISTINCT brand COLLATE "POSIX" FROM products ORDER BY brand;