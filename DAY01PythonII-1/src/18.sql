/*
    Мы хотим изучить, в какие дни у нас 
    было больше всего продаж. 
    Выведите пары дата - количество 
    продаж в те дни, когда их было 
    больше 186000.
*/
SELECT sale_date, COUNT(product_id) as sales_count FROM sales
    GROUP BY sale_date
    HAVING COUNT(product_id) > 186000;