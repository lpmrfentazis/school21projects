/*
    Выведите количество дней, 
    в которые количество продаж было больше 186000.

*/
SELECT count(*) FROM (SELECT COUNT(sale_date) FROM sales GROUP BY sale_date HAVING COUNT(*) > 1860) as t;