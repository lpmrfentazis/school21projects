/*
    Выведите все уникальные пары product_id - 
    store_id, которые купили зарегистрированные 
    пользователи 14 февраля 2020 года.
*/
SELECT DISTINCT product_id, store_id FROM sales WHERE sale_date = '2020-02-14' and customer_id is not null;
