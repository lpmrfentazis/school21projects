/*
    Мы разделили регионы по возрастанию номера 
    региона на тройки, чтобы провести на них 
    проверку бизнес-гипотез. 
    Выведите третью тройку номеров регионов.
*/
--                                                                   group number
SELECT region FROM stores GROUP BY region RDER BY region DESC LIMIT (3-1) OFFSET 3;