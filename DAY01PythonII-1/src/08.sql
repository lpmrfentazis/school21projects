/*
    Выведите самую позднюю дату, 
    на которую хотя бы для одного товара 
    установлена цена. 
    Для решения воспользуйтесь базовыми 
    операторами.
*/
SELECT DISTINCT end_date  FROM prices ORDER BY end_date DESC LIMIT 1