/*
    Выведите все данные по ценам, 
    которые действовали в течение 
    февраля 2020 года 
    (учтите оба поля start_date и end_date).
*/

SELECT * FROM prices WHERE EXTRACT(MONTH FROM start_date) = 2 AND EXTRACT(MONTH FROM end_date) = 2 AND EXTRACT(YEAR FROM start_date) = 2020 AND EXTRACT(YEAR FROM end_date) = 2020;