mes = input().split(", ")

mostCommon = {i: mes.count(i) for i in mes}

for pair in sorted(mostCommon.items(), key=lambda pair: pair[1], reverse=True)[:3]:
    print(f"{pair[0]}: {pair[1]}")