from datetime import datetime, timedelta
from flask import Flask, abort, jsonify, render_template, redirect, request, Response 
from logging import  getLogger
from LorettLogger import LorettLogger, SPAM, VERBOSE, DEBUG, INFO, WARNING
from database import Database

from pathlib import Path
from os import getcwd

import json




__version__ = "0.1"

config = {
    "name": "testServer",
    "dbUser": "vannahyd",
    "loggingLevel": "info",
    "logPath": "./log",
    "workingPath": ".",
}


# IDK
def parseConfig(conf) -> dict:
    outputConfig = {}
    messages = []

    if isinstance(conf, (str, Path)):
        if Path(conf).exists():
            with open(conf) as file:
                outputConfig = json.load(file)

            if outputConfig.keys() == config.keys():
                messages.append(("info", f"Config loaded from {conf}"))

            else:
                outputConfig = config

                messages.append(("warning", f"Incorrect config in {conf}"))
                messages.append(("info", "Load default config"))

                with open(conf, 'w') as f:
                    f.write(json.dumps(config, indent=1))
        else:
            outputConfig = config

            messages.append(("warning", f"Incorrect config in {conf}"))
            messages.append(("info", "Load default config"))

            with open(conf, 'w') as f:
                f.write(json.dumps(config, indent=4))

    elif isinstance(conf, dict):
        if conf.keys() == config.keys():
            outputConfig = conf
            messages.append(("info", f"Config loaded from {conf}"))

        else:
            outputConfig = config

            messages.append(("warning", f"Incorrect config in {conf}"))
            messages.append(("info", "Load default config"))

    else:
        outputConfig = config

        messages.append(("warning", f"Incorrect config in {conf}"))
        messages.append(("info", "Load default config"))

    return outputConfig, messages


class Server:
    def __init__(self, config: dict, messages: list) -> None:
        if config["workingPath"] == ".":
            config["workingPath"] = getcwd()

        self.name = config["name"]
        self.logPath = Path(config["logPath"])
        self.workingPath = Path(config["workingPath"])

        getLogger('werkzeug').setLevel(WARNING)
        self.logger: LorettLogger = LorettLogger(self.name, self.logPath, level=config["loggingLevel"])

        if len(messages):
            for type, mes in messages:
                if (type == "info"):
                    self.logger.info(mes)

                if (type == "warning"):
                    self.logger.warning(mes)

        self.app = Flask(self.name, 
                         template_folder=self.workingPath / "templates", 
                         static_folder= self.workingPath / "static")
        self.app.config['JSON_AS_ASCII'] = False

        self.db: Database = Database(user=config["dbUser"], logger=self.logger)

        self.addEndPoint('/', 'start', self.start, methods=['GET'])
        self.addEndPoint('/home', 'home', self.index, methods=['GET'])
        self.addEndPoint('/index', 'index', self.index, methods=['GET'])
        
        self.addEndPoint('/customers/show/', 'getRandomCustomers', self.getRandomCustomers, methods=['GET'])
        self.addEndPoint('/stores/<int:storeID>', 'getInfoAboutStore', self.getInfoAboutStore, methods=['GET'])
        self.addEndPoint('/prices/max/', 'getMaxPriceProductInfo', self.getMaxPriceProductInfo, methods=['GET'])
        self.addEndPoint('/prices/stats/<int:productID>', 'getProductStat', self.getProductStat, methods=['GET'])
        
        self.addEndPoint('/stores/add', 'addNewStore', self.addNewStore, methods=['POST'])
        self.addEndPoint('/stores/delete/<int:storeID>', 'removeStore', self.removeStore, methods=['POST'])

        # Temp for log source
        @self.app.before_request
        def logRequest():
            self.logger.debug(f"{request.method} - {request.url}")
            self.logger.spam(f"{request.method} - {request.url} from {request.remote_addr}")

    def start(self) -> Response:
        return redirect("/home")

    def index(self) -> str:
        now = datetime.utcnow()
        return render_template("index.html", name=self.name,
                                             time = now.strftime("%H:%M:%S UTC"),
                                             date = now.strftime("%d.%m.%Y")) 

    def getRandomCustomers(self) -> str:
        """
            По запросу `/customers/show/` верните список 10 случайных клиентов в виде:
                [{"customer_id": 1,
                  "name": "Василий",
                  "surname": "Петров"}]
        """
        if request.method == "GET":
            jsonResp = [ {"customer_id": customer.customer_id,
                          "name": customer.name,
                          "surname": customer.surname}
                for customer in self.db.getCustomers(count=10, random=True)]
            
            return jsonify(jsonResp)

    
    def getInfoAboutStore(self, storeID: int) -> str:
        """
            По запросу `/stores/<store_id>` верните всю информацию по магазину 
                с переданным store_id. 
            Названия колонок должны быть ключами JSON-объекта, 
                значения - значениями. 
            Обратите внимание, что в предыдущем задании нужно возвращать 
                список словарей, а здесь - словарь. 

            Если магазина с таким айдишником нет в базе, верните пустой словарь.
        """
        if request.method == "GET":
            store = self.db.getStoreByID(storeID=storeID)
            print(store)

            jsonResp = {"store_id": store.store_id,
                        "address": store.address,
                        "region": store.region} if store else {}

            return jsonify(jsonResp)

    
    def getMaxPriceProductInfo(self) -> str:
        """
            По запросу `/prices/max` верните всю информацию о товаре, 
                цена которого была максимальной за всё время, 
                а также саму цену и дату начала ее действия. 
            Если таких товаров несколько, 
                выведите тот, максимальная цена которого была назначена позже.
        """
        if request.method == "GET":
            product = self.db.getMaxPriceProduct()

            jsonResp = {"product_id": product.product_id,
                        "name": product.name,
                        "category": product.category,
                        "brand": product.brand} if product else {}

            return jsonify(jsonResp)

    
    def getProductStat(self, productID: int) -> str:
        """
            По запросу `/prices/stats/<product_id>` для продукта с переданным product_id 
                выведите статистику по ценам в следующем виде:

                {
                    "count": <число - количество цен по этому объекту в базе>,
                    "stores_count": <число - количество магазинов, в которых этот товар хоть раз продавался (без дублей)>,
                    "max_price": <число - максимальная цена по этому товару>,
                    "min_price": <число - минимальная цена по этому товару>,
                    "avg_price": <число - средняя цена по всем магазинам по данному товару>
                }
    
            Если продукта с этим product_id в базе нет, верните пустой словарь.
        """
        if request.method == "GET":
            stat = self.db.getProductStatByID(productID=productID)
            jsonResp = {"count": stat.count,
                        "stores_count": stat.storesCount,
                        "max_price": stat.maxPrice,
                        "min_price": stat.minPrice,
                        "avg_price": stat.avgPrice} if stat else {}

            return jsonify(jsonResp)

    
    def addNewStore(self) -> str:
        """
            По запросу `/stores/add` добавьте новый магазин в таблицу 
                магазинов. 
            Вид входящих данных в POST-запросе:
                {
                    "address": "ул. Пушкина, д. 5",
                    "region": 5
                }
            Запрос должен вернуть `{"store_id": 25}` с тем айдишником, 
                который БД присвоила новому магазину.
        """
        if request.method == "POST":
            data = request.get_json()
            address = data['address']
            region = data['region']
            
            if address and region:
                store = self.db.addNewStore(address=address, region=region)
            else:
                abort(400, "Address and region not recognized")

            return jsonify({"store_id": store.store_id})
    

    def removeStore(self, storeID:int) -> str:
        """
            По запросу `/stores/delete/<store_id>` 
                удалите из базы магазин с поступившим айдишником. 
                
            Если всё прошло гладко, верните словарь 
                {"status": "ok"}. 

            Если такого магазина в базе нет, 
                верните ответ `{"status": "not found"}`.
        """
        if request.method == "POST":
            
            return jsonify({"status": "ok" if self.db.removeStore(storeID) 
                                            else "not found"})


    def addEndPoint(self, endpoint=None, endpoint_name=None, handler=None, methods=['GET'], *args, **kwargs) -> None:
        self.app.add_url_rule(endpoint, endpoint_name, handler, methods=methods)


    def run(self, **kwargs):
        self.app.run(**kwargs)



if __name__ == "__main__":
    config, messages = parseConfig(Path(__file__).parents[0] / "config.json")

    level = DEBUG

    if "info" in config['loggingLevel'].lower():
        config['loggingLevel'] = INFO

    elif "debug" in config['loggingLevel'].lower():
        config['loggingLevel'] = DEBUG

    elif "warning" in config['loggingLevel'].lower():
        config['loggingLevel'] = WARNING

    elif "spam" in config['loggingLevel'].lower():
        config['loggingLevel'] = SPAM

    elif "verbose" in config['loggingLevel'].lower():
        config['loggingLevel'] = VERBOSE

    server = Server(config=config, messages=messages)
    server.run(host="0.0.0.0", port=8080, debug=True)
