from sqlalchemy import Integer, String, Column, DATE
from sqlalchemy.orm import declarative_base, relationship


__all__ = ["Store", "Product", "Customer", "Price", "Sale"]


Base = declarative_base()

class Store(Base):
    __tablename__ = 'stores'
    
    store_id = Column(Integer, autoincrement=True, primary_key=True)
    address = Column(String(255), nullable=False)
    region = Column(Integer, nullable=False)

    def __str__(self) -> str:
        return f"Store(store_id={self.store_id}, address={self.address}, region={self.region})"

class Product(Base):
    __tablename__ = 'products'

    product_id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String(255), nullable=False)
    category = Column(String(255), nullable=False)
    brand = Column(String(255), nullable=False)

    def __str__(self) -> str:
        return f"Product(product_id={self.product_id}, name={self.name}, category={self.category}, brand={self.brand})"

class Customer(Base):
    __tablename__ = 'customers'

    customer_id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String(255), nullable=False)
    surname = Column(String(255), nullable=False)
    birth_date = Column(DATE, nullable=False)

    def __str__(self) -> str:
        return f"Customer(customer_id={self.customer_id}, name={self.name}, surname={self.surname}, birth_date={self.birth_date})"

class Price(Base):
    __tablename__ = 'prices'

    price_id = Column(Integer, autoincrement=True, primary_key=True)
    product_id = Column(Integer, nullable=False)
    price = Column(Integer, nullable=False)
    start_date = Column(DATE, nullable=False)
    end_date = Column(DATE, nullable=False)

    def __str__(self) -> str:
        return f"Price(price_id={self.price_id}, product_id={self.product_id}, price={self.price}, start_date={self.start_date}, end_date={self.end_date})"

class Sale(Base):
    __tablename__ = 'sales'

    sale_id = Column(Integer, autoincrement=True, primary_key=True)
    product_id = Column(Integer, nullable=False)
    store_id = Column(Integer, nullable=False)
    customer_id = Column(Integer, nullable=False)
    sale_date = Column(DATE, nullable=False)

    def __str__(self) -> str:
        return f"Sale(sale_id={self.sale_id}, product_id={self.product_id}, store_id={self.store_id}, customer_id={self.customer_id}, sale_date={self.sale_date})"
