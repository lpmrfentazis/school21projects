from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import MetaData, Table, String, Integer, Column, ForeignKey
from sqlalchemy.sql.expression import and_, func
from random import randint
from dataclasses import dataclass
from verboselogs import VerboseLogger

from dbModel import *


# TODO SESSIONS POOL

@dataclass
class ProductStat:
    count: int
    storesCount: int
    maxPrice: float
    minPrice: float
    avgPrice: float

class Database:
    def __init__(self, user: str, logger: VerboseLogger) -> None:
        self.logger = logger

        self.logger.info("Database init")

        self.user = user
        self.engine = create_engine(f"postgresql+psycopg2://{self.user}:@localhost:5432/")
        self.session = sessionmaker(bind=self.engine)()


    def getCustomers(self, count: int, random: bool = False) -> list[Store]:
        rownCount = self.session.query(func.count(Customer.customer_id)).scalar()
        
        if not rownCount:
            return []
        elif rownCount < count:
            count = rownCount

        offset = randint(0, rownCount-count) if random else 0

        result = self.session.query(Customer).offset(offset).limit(count).all()
        
        self.logger.debug(f"Database getCustomers({count=}, {random=})")
        self.logger.spam(f"Database getCustomers({count=}, {random=}) -> {result}")

        return result


    def getStoreByID(self, storeID: int) -> (Store | None):
        result = self.session.query(Store).filter(Store.store_id == storeID).first()
        
        self.logger.debug(f"Database getStoreByID({storeID=})")
        self.logger.spam(f"Database getStoreByID({storeID=}) -> {result}")

        return result


    def getMaxPriceProduct(self) -> (Product | None):
        # i hate syntax from example
        productId = self.session.query(Price.product_id)
        productId = productId.order_by(Price.price.desc())
        productId = productId.order_by(Price.start_date.desc()).first()

        result = self.session.query(Product).filter(Product.product_id == productId).first()

        self.logger.debug(f"Database getMaxPriceProduct()")
        self.logger.spam(f"Database getMaxPriceProduct() -> {result}")

        return result

    def getProductStatByID(self, productID: int) -> (ProductStat | None):
        res = self.session.query(func.count(Price.price_id),
                                 func.max(Price.price),
                                 func.min(Price.price),
                                 func.avg(Price.price))\
                                    .filter(Price.product_id == productID)\
                                    .group_by(Price.product_id).first()
        
        storesCount = self.session.query(Sale.store_id).filter(Sale.product_id == productID).distinct().count()
        
        if res and storesCount:
            result = ProductStat(count=res[0],
                                storesCount=storesCount,
                                maxPrice=res[1],
                                minPrice=res[2],
                                avgPrice=res[3]) 
        else: result = None

        self.logger.debug(f"Database getProductStatByID({productID=})")
        self.logger.spam(f"Database getProductStatByID({productID=}) -> {result}")

        # !
        return result  

    def addNewStore(self, address: str, region: int):
        # Add collision check? 
        store = Store(address=address, region=region) # store.id = None

        self.session.add(store)
        self.session.commit()

        self.logger.debug(f"Database addNewStore({address=}, {region=})")
        self.logger.spam(f"Database addNewStore({address=}, {region=}) -> {store}")

        return store # store.id != None

    def removeStore(self, storeID: int) -> bool:
        store = self.session.query(Store).filter(Store.store_id == storeID).first()

        if store:
            self.session.delete(store)
            self.session.commit()
            result = True

        else: result = False

        self.logger.debug(f"Database removeStore({storeID=})")
        self.logger.spam(f"Database removeStore({storeID=}) -> {store}")

        return result

        

if __name__ == "__main__":
    ...
   #print(Database("vannahyd").removeStore(650))