from verboselogs import VerboseLogger
from datetime import datetime

try:
    from database.models import *    
    from database.database import Database
    
except ModuleNotFoundError:
    from models import *    
    from database import Database


exampleUser = User(userID=0, name="Andrew", surname="Pivovarov", rank=2)

exampleAuthor = Author(authorID=0,
                        authorName="Альбер Камю", 
                        birthday=datetime(year=1913, month=11, day=7))

exampleBook = Book(bookID=0, 
                    bookName="Посторонний", 
                    authorID=0,
                    year=2020,
                    donatedBy=0)

db = Database(VerboseLogger(__name__))

# admin / staff
def getUserByID(userID: int) -> User:
    return db.getUserByID(userID)

def getUserByTelegramID(telegramID: int) -> User:
    return db.getUserByTelegramID(telegramID)


def getAuthorByID(authorID: int) -> Author:
    return db.getAuthorByID(authorID)

def updateBookUsedBy(bookID: int, userID: int) -> Book:
    return db.updateBookUsedBy(bookID, userID)

def getBookByID(bookID: int) -> Book:
    return db.getBookByID(bookID)

def getBooksLike(query: str) -> list[Book]:
    return db.getBooksLike(query)

def getBooksByAuthor(authorID) -> list[Book]:
    return db.getBookByAuthor(authorID)

def getAuthorsLike(query: str) -> list[Author]:
    return db.getAuthorsLike(query)

def returnBookByID(bookID: int, userID: int) -> Book:
    return db.updateBookUsedBy(bookID, userID)

def removeBookByID(bookID: int) -> bool:
    return db.removeBookByID(bookID)

def removeUserByID(userID: int) -> bool:
    return db.removeUserByID(userID)

def removeAuthorByID(authorID: int) -> bool:
    return db.removeAuthorByID(authorID)


def initExamplesDatabase() -> None:
    andrew = db.addUser("Andrew", "Pivovarov", "@DedAndrew", 2)
    mikhail = db.addUser("Mikhail", "Popechitelev", "@popech", 0)
    alexey = db.addUser("Alexey", "Poleno", "@poleno", 0)
    abobus = db.addUser("Abobus", "Ryajeniy", "@Obobubaba", 1)
    
    kamue = db.addAuthor("Alber Kamue", datetime(1913, 11, 7))
    andreev = db.addAuthor("Leonid Andreev", datetime(1871, 8, 21))
    gluhovskiy = db.addAuthor("Dmitriy Gluhovskiy", datetime(1979, 7, 12))
    
    
    outsider = db.addBook("Outsider", kamue.authorID, donatedBy=andrew.userID, usedBy=None, year=2020)
    redLaught = db.addBook("Red Laught", andreev.authorID, donatedBy=andrew.userID, usedBy=None, year=2022)
    metro = db.addBook("Metro2033", gluhovskiy.authorID, donatedBy=andrew.userID, usedBy=None, year=2016)
    
    metro1 = db.addBook("Metro2033", gluhovskiy.authorID, donatedBy=alexey.userID, usedBy=None, year=2016)
    
    db.addTransaction(metro.bookID, mikhail.userID, datetime.utcnow(), None, 7)
    db.addTransaction(redLaught.bookID, alexey.userID, datetime.utcnow(), datetime.now(), 7)
    db.addTransaction(redLaught.bookID, alexey.userID, datetime.utcnow(), None, 7)
    

if __name__ == "__main__":
    initExamplesDatabase()
