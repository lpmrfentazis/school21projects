from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import func
from sqlalchemy_utils import database_exists, create_database
from typing import Union
from dataclasses import dataclass

try:
    from database.models import *
except ModuleNotFoundError:
    from models import *    

from verboselogs import VerboseLogger
from datetime import datetime



    
    
@dataclass
class BookStatictics:
    bookID: int
    bookName: str
    authorName: str
    year: int
    usedBy: str
    start: datetime
    end: datetime

class Database:
    def __init__(self, logger: VerboseLogger, user: str="postgres") -> None:
        self.logger = logger

        self.logger.info("Database init")

        self.user = user
        self.engine = create_engine(f"postgresql+psycopg2://{self.user}:{'784596321'}@localhost:5432/test")
        if not database_exists(self.engine.url):
            create_database(self.engine.url)
        self.session = sessionmaker(bind=self.engine)()
        
        Base.metadata.create_all(self.engine)
    
    def getBookByID(self, bookID: int) -> Union[Book, None]:
        return self.session.query(Book).filter(Book.bookID == bookID).first()
    
    def getBooksLike(self, query: str) -> list[Book]:
        return self.session.query(Book).filter(Book.bookName.like(f"%{query}%")).all()
    
    def getBookByAuthor(self, authorID: int) -> list[Book]:
        return self.session.query(Book).filter(Book.authorID == authorID).all()
        
    def addBook(self, bookName: str, authorID: int, donatedBy: int, usedBy: int, year: int) -> Union[Book, None]:
        try:
            if self.getUserByID(donatedBy)  is None:
                return None
            if usedBy is not None:
                if self.getUserByID(usedBy) is None:
                    return None
            if self.getAuthorByID(authorID) is None:
                return None
            
                
            book = Book(bookName=bookName, authorID=authorID, donatedBy=donatedBy, usedBy=usedBy, year=year)
            
            self.session.add(book)
            self.session.commit()
            
            return book
            
        except:
            return None
        
    def updateBookUsedBy(self, bookID: int, userID: int) -> Union[Book, None]:
        try:
            book = self.getBookByID(bookID)
            user = self.getUserByID(userID)
            
            if book is None:
                return None
            
            if userID is not None:
                if user is None:
                    return None
            
            # banned
            if user.rank == -1:
                return None
            
            if userID is None:
                transaction = self.getTransactionByID(book)
                self.setTransactionEnd()
            
            book.usedBy = user.userID
            
            self.session.commit()
            
            return book
            
        except:
            return None
        
    def removeBookByID(self, bookID: int) -> bool:
        try:
            book = self.getBookByID(bookID)
            
            if book is None:
                return False
            
            self.session.delete(book)
            self.session.commit()
            
            return True
            
        except:
            return False
        
    def getAuthorByID(self, authorID: int) -> Union[Author, None]:
        return self.session.query(Author).filter(Author.authorID == authorID).first()
    
    def getAuthorByName(self, authorName: str) -> Union[Author, None]:
        return self.session.query(Author).filter(Author.authorName == authorName).first()
    
    def getAuthorsLike(self, query: str) -> list[Book]:
        return self.session.query(Author).filter(Author.authorName.like(f"%{query}%")).all()
        
    def addAuthor(self, authorName: str, birthday: datetime) -> Union[Author, None]:
        try:
            author = self.getAuthorByName(authorName)
            
            if author is not None:
                return author
            
            author = Author(authorName=authorName, birthday=birthday)
            
            self.session.add(author)
            self.session.commit()
            
            return author
            
        except:
            return None
        
    def removeAuthorByID(self, authorID: int) -> bool:
        try:
            author = self.getAuthorByID(authorID)
            
            if author is None:
                return False
            
            self.session.delete(author)
            self.session.commit()
            
            return True
            
        except:
            return False
    
    def getUserByID(self, userID: int) -> Union[User, None]:
        return self.session.query(User).filter(User.userID == userID).first()
    
    def getUserByTelegramID(self, telegramID: int) -> Union[User, None]:
        return self.session.query(User).filter(User.telegramID == telegramID).first()
        
    def addUser(self, name: str, surname: str, telegramID: str, rank: int) -> Union[User, None]:
        try:
            user = self.getUserByTelegramID(telegramID=telegramID)
            if user is not None:
                return user
            
            user = User(name=name, surname=surname, telegramID=telegramID, rank=rank)
            
            self.session.add(user)
            self.session.commit()
            
            return User
            
        except Exception as e:
            raise e
            return None
        
    def updateUserByID(self, userID: int, name: str, surname: str, telegramID: str, rank: int) -> Union[User, None]:
        try:
            user = self.getUserByID(userID)
            
            if user is None:
                return False
            
            user.name = name
            user.surname = surname
            user.telegramID = telegramID
            user.rank = rank
            
            self.session.commit()
            return User
            
        except:
            return None
        
    def removeUserByID(self, userID: int) -> bool:
        try:
            user = self.getUserByID(userID)
            
            if user is None:
                return False
            
            self.session.delete(user)
            self.session.commit()
            
            return True
            
        except:
            return False
    
    def getTransactionByUserID(self, userID: int):
        return self.session.query(Transaction).filter(Transaction.userID == userID).first()
        
    def getTransactionByID(self, transactionID: int) -> Union[Transaction, None]:
        return self.session.query(Transaction).filter(Transaction.transactionID == transactionID).first()
    
    def getTransactionsByBookID(self, bookID: int) -> list[Transaction]:
        result =  self.session.query(Transaction).filter(Transaction.bookID == bookID).all()
        ans = []
        
        for trans in result:
            book = self.getBookByID(trans.bookID)
            user = self.getUserByID(trans.userID)
            author = self.getAuthorByID(book.authorID)
            ans.append(BookStatictics(trans.bookID, book.bookName, author.authorName, book.year, f"user.name"))    
        
    def addTransaction(self, bookID: int, userID: int, start: datetime, end: datetime, period: int) -> Union[Transaction, None]:
        try:
            book = self.getBookByID(bookID)
            if book is None:
                return None
            
            if book.usedBy is not None: 
                return None
            
            user = self.getUserByID(userID)
            if user is None:
                return None
            
            # banned
            if user.rank == -1:
                return None
            
            if end is not None:
                if start > end:
                    return None
            
            if period <= 0:
                return None
            
            if self.updateBookUsedBy(bookID, userID) is None:
                return None
            
            #if self.session.query(Transaction).filter(Transaction.bookID == bookID)
            
            transaction = Transaction(bookID=bookID, userID=userID, start=start, end=end, period=period)
            
            self.session.add(transaction)
            self.session.commit()
            
            return transaction
            
        except:
            return None
        
    def setTransactionEnd(self, transactionID: int, end: datetime) -> Union[Transaction, None]:
        try:
            transaction = self.session.query(Transaction).filter(Transaction.transactionID == transactionID)
            
            if transaction.start >= end:
                return False
            
            transaction.end = end
            
            self.session.commit()
            
            return transaction
            
        except:
            return None
        
    def removeTransaction(self, transactionID: int) -> bool:
        try:
            transaction = self.getTransactionByID(transactionID)
            
            if transaction is None:
                return False
            
            self.session.delete(transaction)
            self.session.commit()
            
            return True
            
        except:
            return False
        
        
if __name__ == "__main__":
    db = Database(VerboseLogger(__name__))