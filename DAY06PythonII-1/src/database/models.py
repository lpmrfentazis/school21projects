from sqlalchemy import Integer, String, Column, Date, DateTime, ForeignKey
from sqlalchemy.orm import declarative_base
from datetime import datetime
from dataclasses import dataclass



__all__ = ["User", "Book", "Author", "Transaction", "Base"]


Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    userID = Column(Integer, autoincrement=True, primary_key=True)
    
    name = Column(String(250), nullable=False)  
    surname = Column(String(250), nullable=False) 
    telegramID = Column(String(250), nullable=False, unique=True)

    # 0 - reader
    # 1 - staff
    # 2 - admin 
    # -1 - banned
    rank = Column(Integer, nullable=False)

class Author(Base):
    __tablename__ = "authors"

    authorID = Column(Integer, autoincrement=True, primary_key=True)
    authorName = Column(String(250), nullable=False)
    birthday = Column(Date, nullable=True)

class Book(Base):
    __tablename__ = "books"

    bookID = Column(Integer, autoincrement=True, primary_key=True)
    
    # about book
    bookName = Column(String(250), nullable=False)
    authorID = Column(Integer, ForeignKey("authors.authorID"), nullable=False)

    # about users
    donatedBy = Column(Integer, ForeignKey("users.userID"), nullable=False)
    usedBy = Column(Integer, ForeignKey("users.userID"), nullable=True)
    year = Column(Integer, nullable=False)


class Transaction(Base):
    __tablename__ = "transactions"

    transactionID = Column(Integer, autoincrement=True, primary_key=True)
    
    bookID = Column(Integer, ForeignKey("books.bookID"), nullable=False)
    userID = Column(Integer, ForeignKey("users.userID"), autoincrement=True)
    
    start = Column(DateTime, default=datetime.utcnow, nullable=False)
    end = Column(DateTime, nullable=True)
    period = Column(Integer, nullable=True)
