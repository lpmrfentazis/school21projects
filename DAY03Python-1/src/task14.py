class Calculator:
    """
        Comments for this class are useless, as is this class itself.
        doxygen and other technologies don't make sense here

        u can use .history(self, n: int) method for get one of the last operations this 
        like history[-n]. Its only for u object

        and u can use last property for get last operation for all Calculator objects 
    """ 
    last = None

    def __init__(self) -> None:
        self._history: list = []

    def sum(self, a: float, b: float) -> float:
        res = a + b
        
        action = f"sum({a}, {b}) == {round(res, 1)}"
        Calculator.last = action
        
        self._history.append(action)

        return res

    def sub(self, a: float, b: float) -> float:
        res = a - b

        action = f"sub({a}, {b}) == {round(res, 1)}"
        Calculator.last = action
        
        self._history.append(action)
        
        return res

    def mul(self, a: float, b: float) -> float:
        res = a * b
        
        action = f"mul({a}, {b}) == {round(res, 1)}"
        Calculator.last = action
        
        self._history.append(action)
        
        return res

    def div(self, a: float, b: float, mod: bool = False) -> float:
        if mod:
            res = a % b
        else:
            res = a / b

        action = f"div({a}, {b}) == {round(res, 1)}"
        Calculator.last = action
        
        self._history.append(action)
        
        return res

    def history(self, n: int):
        if not isinstance(n, int):
            raise ValueError("The index value can only be an integer")

        if n <= len(self._history):
            return self._history[len(self._history) - n]

        return None

    @classmethod
    def clear(cls):
        cls.last = None