from __future__ import annotations


class BaseWallet:
    """
        Base Wallet class, you can use him for create new wallet:
        You only need to redefine the __str__ method, and set new exchange_rate

        example: 
            class WrapperWallet(BaseWallet):
                def __init__(self, name: str, amount: float) -> None:
                    super().__init__(name, amount)
                    self.exchange_rate = 8

                def __str__(self) -> str:
                    return f"Wrapper Wallet {self.name} {self.amount}"

        Reset the wallet: spend_all() -> None
        Convert to base wallet: to_base() -> float

        you can use operators:
            operator+ with int, float, wallet

            operator+= with int, float, wallet

            operator- with int, float, wallet

            operator-= with int, float, wallet

            operator* with int, float

            operator*= with int, float

            operator/ with int, float

            operator/= with int, float

            operator== with int, float, wallet

            operator!= with int, float, wallet


        And rvalue operators:


            operator+ with int, float

            operator- with int, float

            operator* with int, float

            operator/ with int, float

    """
    name: str = ""
    amount: float = 0
    exchange_rate: float = 1

    def __init__(self, name: str, amount: float) -> None:
        self.name = name
        self.amount = amount

    def spend_all(self) -> None:
        if self.amount > 0:
            self.amount = 0

    def to_base(self) -> float:
        return self.amount * self.exchange_rate

    def __str__(self) -> str:
        return f"Base Wallet {self.name} {self.amount}"
    
    def __add__(self, value: int | float | BaseWallet):
        # It doesn't look very nice, 
        # but the functionality pleases all subsequent heirs of Base Wallet, 
        # without the need for manual addition

        if isinstance(value, (int, float)):
            # Create new object like self
            return type(self)(name=self.name, amount=self.amount + value)

        elif isinstance(value, BaseWallet):
            # Create new object like self
            return type(self)(name=self.name, amount=self.amount + value.to_base() / self.exchange_rate)

        raise TypeError(f"Base Wallet supports addition only with float, int, BaseWallet.  But given {type(value)}")

    def __iadd__(self, value: int | float | BaseWallet):
        if isinstance(value, (int, float)):
            self.amount += value
            return self

        elif isinstance(value, BaseWallet):
            self.amount += value.to_base() / self.exchange_rate
            return self

        raise TypeError(f"Base Wallet supports addition only with float, int, BaseWallet.  But given {type(value)}")
    
    def __radd__(self, value: int | float):
        if isinstance(value, (int, float)):
            return self + value

        raise TypeError(f"Base Wallet supports addition only with float, int.  But given {type(value)}")

    def __sub__(self, value: int | float | BaseWallet):
        # It doesn't look very nice, 
        # but the functionality pleases all subsequent heirs of Base Wallet, 
        # without the need for manual addition

        if isinstance(value, (int, float)):
            # Create new object like self
            return type(self)(name=self.name, amount=self.amount - value)

        elif isinstance(value, BaseWallet):
            # Create new object like self
            return type(self)(name=self.name, amount=self.amount - value.to_base() / self.exchange_rate)

        raise TypeError(f"Base Wallet supports subtraction only with float, int, BaseWallet.  But given {type(value)}")

    def __isub__(self, value: int | float | BaseWallet):
        if isinstance(value, (int, float)):
            self.amount -= value
            return self

        elif isinstance(value, BaseWallet):
            self.amount -= value.to_base()  / self.exchange_rate
            return self

        raise TypeError(f"Base Wallet supports subtraction only with float, int, BaseWallet.  But given {type(value)}")
    
    def __rsub__(self, value: int | float):
        if isinstance(value, (int, float)):
            return type(self)(name=self.name, amount=-self.amount) + value

        raise TypeError(f"Base Wallet supports subtraction only with float, int, BaseWallet.  But given {type(value)}")

    def __mul__(self, value: int | float):
        # It doesn't look very nice, 
        # but the functionality pleases all subsequent heirs of Base Wallet, 
        # without the need for manual addition

        if isinstance(value, (int, float)):
            # Create new object like self
            return type(self)(name=self.name, amount=self.amount * value)
        
        raise TypeError(f"Base Wallet supports multiplication only with float, int.  But given {type(value)}")

    def __imul__(self, value: int | float):
        if isinstance(value, (int, float)):
            self.amount *= value
            return self
        
        raise TypeError(f"Base Wallet supports multiplication only with float, int.  But given {type(value)}")

    def __rmul__(self, value: int | float):
        if isinstance(value, (int, float)):
            return type(self)(name=self.name, amount=self.amount * value)

        raise TypeError(f"Base Wallet supports multiplication only with float, int.  But given {type(value)}")

    def __truediv__(self, value: int | float):
        # It doesn't look very nice, 
        # but the functionality pleases all subsequent heirs of Base Wallet, 
        # without the need for manual addition

        if isinstance(value, (int, float)):
            # Create new object like self
            return type(self)(name=self.name, amount=self.amount / value)
        
        raise TypeError(f"Base Wallet supports divivnity only with float, int.  But given {type(value)}")

    def __idiv__(self, value: int | float):
        if isinstance(value, (int, float)):
            self.amount /= value
            return self

        raise TypeError(f"Base Wallet supports divivnity only with float, int.  But given {type(value)}")

    def __rtruediv__(self, value: int | float):
        if isinstance(value, (int, float)):
            return type(self)(name=self.name, amount=self.amount / value)

        raise TypeError(f"Base Wallet supports divivnity only with float, int.  But given {type(value)}")

    def __eq__(self, other: BaseWallet) -> bool:
        if isinstance(other, BaseWallet):
            return type(self) == type(other) and self.amount == other.amount
        
        return False

    def __ne__(self, other: BaseWallet):
        return not (self == other)
        

class RubbleWallet(BaseWallet):
    def __init__(self, name: str, amount: float) -> None:
        super().__init__(name, amount)
        self.exchange_rate = 1

    def __str__(self) -> str:
        return f"Rubble Wallet {self.name} {self.amount}"


class DollarWallet(BaseWallet):
    def __init__(self, name: str, amount: float) -> None:
        super().__init__(name, amount)
        self.exchange_rate = 60
    
    def __str__(self) -> str:
        return f"Dollar Wallet {self.name} {self.amount}"


class EuroWallet(BaseWallet):
    def __init__(self, name: str, amount: float) -> None:
        super().__init__(name, amount)
        self.exchange_rate = 70

    def __str__(self) -> str:
        return f"Euro Wallet {self.name} {self.amount}"