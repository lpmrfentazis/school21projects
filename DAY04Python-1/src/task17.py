import pickle
import json
from requests import get, post

urlInput = "https://jsonplaceholder.typicode.com/"
urlOutput = "https://webhook.site/3d9f2240-7ef9-4551-be59-d2ea9e3774d0"


def getUsers() -> list[dict]:
    response = get(urlInput + "users")
    code = response.status_code
    if code != 200:
        return (code, {})

    usersData = [
            { key: i[key] for key in ["id", "username", "email"]} 
            for i in response.json()]

    return (code, usersData)


def getPostsData(users: list[dict]) -> list[dict]:
    # users - getUsers result
    # return list dicts like that:
    # {
    #      "id": 1,
    #      "username": "lolkek",
    #      "email": "user1@mail.dot",
    #      "posts": 125,
    #      "comments": 1358
    #}
    # I started making a beautiful solution through dataclasses, 
    # but at some point I realized that the task was too 'Попущенная' to do it beautifully
    
    response = get(urlInput + "posts")
    code = response.status_code

    if code != 200:
        return (code, [])
    
    posts = response.json()

    response = get(urlInput + "comments")
    code = response.status_code

    if code != 200:
        return (code, [])
    
    comments = response.json()


    for user in range(len(users)):
        targetPosts = [ i["id"] for i in posts if i["userId"] == users[user]["id"]]
        users[user]["posts"] = len(targetPosts)
        
        commentsCount = len([ 1 for comment in comments if comment["postId"] in targetPosts])
        users[user]["comments"] = commentsCount

    return (code, users)

def sendTrash():
    users = getUsers()

    if users[0] != 200:
        return False

    posts = getPostsData(users[1])

    if posts[0] != 200:
        return False

    #pprint({"statistics": posts[1]}, indent=2)
    response = post(urlOutput, json={"statistics": posts[1]})
    code = response.status_code

    
    with open("task17.pickle", 'wb') as f:
        pickle.dump(response, f)

    return True
