from task16 import create_booking
from datetime import datetime, timedelta
import json


def getResponse(result: bool, msg: str, room_name: str, start: datetime, end: datetime) -> str:
    return json.dumps({
      "created": result,
      "msg": msg,
      "booking": {
        "room_name": room_name,
        "start_date": start.strftime("%Y-%m-%d"),
        "start_time": start.strftime("%H:%M"),
        "end_date": end.strftime("%Y-%m-%d"),
        "end_time": end.strftime("%H:%M"),
        "duration": (end.timestamp() - start.timestamp()) // 60
      }
    })

if __name__ == "__main__":

    start = datetime.now()
    end = start + timedelta(minutes=5)

    assert create_booking("Вагнер", start, end) == getResponse(True, "Бронирование создано", "Вагнер", start, end), "Неправильное сообщение при удачной записи"
    assert create_booking("Вагнер1", start, end) == getResponse(False, "Комната не найдена", "Вагнер1", start, end), "Неправильное сообщение при плохом имени комнаты"

    assert create_booking("Вагнер", datetime(2022, 9, 1, 10), datetime(2022, 9, 1, 11)) == getResponse(False, "Комната занята", "Вагнер", datetime(2022, 9, 1, 10), datetime(2022, 9, 1, 11)), "Неправильное сообщение при занятой комнате"
    
    try: 
        create_booking("Вагнер", datetime(2022, 9, 1, 11), datetime(2022, 9, 1, 10))
        assert False, "Booking не бросает ValueError если указать неправильные даты"
    except ValueError:
        ...

    print ("Тестирование пройдено")
    


