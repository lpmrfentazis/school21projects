from api import register_booking
from datetime import datetime
import json


class Booking:
    room_name: str

    __start: datetime
    __end: datetime
    
    __start_date: str
    __end_date: str

    __start_time: str
    __end_time: str

    __duration: int

    def __init__(self, room_name: str, start: datetime, end: datetime) -> None:
        self.__checkDate(start, end)

        self.room_name = room_name
        self.__start = start
        self.__end = end

        self.__preparate()

    def __preparate(self) -> None:
        self.__duration = (self.end.timestamp() - self.start.timestamp()) // 60

        self.__start_date = self.start.strftime("%Y-%m-%d")
        self.__end_date = self.end.strftime("%Y-%m-%d")

        self.__start_time = self.start.strftime("%H:%M")
        self.__end_time = self.end.strftime("%H:%M")

    def __checkDate(self, start: datetime, end: datetime) -> None:
        if start.timestamp() > end.timestamp():
            raise ValueError("The booking start time is later than the end time", 0)
        elif start.timestamp() == end.timestamp():
            raise ValueError("Zero range booking", 1)

    @property
    def duration(self):
        return self.__duration

    @property
    def start_date(self):
        return self.__start_date

    @property
    def start_time(self):
        return self.__start_time

    @property
    def end_date(self):
        return self.__end_date

    @property
    def end_time(self):
        return self.__end_time

    @property
    def start(self) -> datetime:
        return self.__start

    @property
    def end(self) -> datetime:
        return self.__end

    @start.setter
    def start(self, start: datetime) -> None:
        # WTF author 
        # The author of the task requested the ability to replace
        # start and end at any time !SEPARATELY!, 
        # but did not describe when we should check correctness.
        #
        # So now if the user replaces the fields in the wrong order he will get an error
        self.__checkDate(start, self.end)
        self.__start = start
        self.__preparate()

    @end.setter
    def end(self, end: datetime) -> None:
        # WTF author 
        # The author of the task requested the ability to replace
        # start and end at any time !SEPARATELY!, 
        # but did not describe when we should check correctness.
        #
        # So now if the user replaces the fields in the wrong order he will get an error
        
        self.__checkDate(self.start, end)
        self.__end = end
        self.__preparate()

def create_booking(room_name: str, start: datetime, end: datetime) -> str:
    print("Начинаем создание бронирования")
    mes = {
                    "created": False,
                    "msg": "",
                    "booking": {
                        "room_name": room_name,
                        "start_date": start.strftime("%Y-%m-%d"),
                        "start_time": start.strftime("%H:%M"),
                        "end_date": end.strftime("%Y-%m-%d"),
                        "end_time": end.strftime("%H:%M"),
                        "duration": (end.timestamp() - start.timestamp()) // 60
                    }
                }
    
    try:
        booking = Booking(room_name=room_name, start=start, end=end)

        result = register_booking(booking)

        if result:
            mes["created"] = True
            mes["msg"] = "Бронирование создано"
            
        else:
            mes["msg"] = "Комната занята"

    except KeyError as e:
        mes["msg"] = "Комната не найдена"

    finally:
        print("Заканчиваем создание бронирования")

    return json.dumps(mes)
    