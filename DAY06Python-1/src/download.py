# Created by michelem
# Reviewed by vannahyd aka MrFentazis
from requests import get, post

class Downloader:
    """
        A class that receives the specified web page 
        and allows you to save it to a file or receive it as a string
        
        Save to file - save(path: str) -> bool 
        
        Get the text - get_html() -> str
    """
    def __init__(self, url: str, params: dict, method: str = "GET") -> None:
        self.url = url
        self.params = params
        if method == "GET":
            responce = get(url = self.url, params = self.params)
            self.responce = responce.text
        elif method == "POST":
            responce = post(url = self.url, params = self.params)
            self.responce = responce.text
        else:
            raise ValueError("Unknown method: choose POST or GET")
        
        if responce.status_code != 200:
            responce.raise_for_status()

            
    def save(self, path: str) -> bool:
    
        """
            Save to path as .html file
            return True if successful else - False
        """
        try:
            with open(path, "w") as file:
                file.write(self.responce)
        except:
            return False
            
        return True
            
    
        
    def get_html(self) -> str:
        return self.responce
        
        
        
if __name__ == "__main__":
    check_params = "https://eus.lorett.org/eus/logs_frames.html"
    url = "https://www.sports.ru/news/"
    params = {
        "t0": "2023-04-05",
        "t1": "2023-04-09"
    }
    method = "GET" # or POST
    downloader = Downloader(url=url, params=params, method=method)
    
    print(downloader.get_html())
    downloader.save("page.html")