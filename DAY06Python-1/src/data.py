# Created by vannahyd
# Reviewed by vannahyd aka MrFentazis
from .download import Downloader
from .parse import Parser


class Statistics:
    def ___init___(self):
        self.downloader = Downloader("https://www.sports.ru/news/")
        self.downloader.save("parse.html")
        self.parser = Parser("")
        
    def getDataFormJson(self, path: str):
        ...
    
    def getDataFromYaml(self, path: str):
        ...
        