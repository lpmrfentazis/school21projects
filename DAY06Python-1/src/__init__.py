# Created by vannahyd
# Reviewed by vannahyd aka MrFentazis
from download import Downloader
from parse import Parser
from data import Statistics
from tempfile import mkstemp
from prettytable import PrettyTable
from pathlib import Path


__all__ = ["Downdloader", "Parser"]

__all__ = "1.0"

def getSportsNews() -> list[dict]:    
    downloader = Downloader("https://www.sports.ru/news/", params={})    
    file, fileName = mkstemp()
    
    downloader.save(fileName)
    parser = Parser(fileName)
    
    # Remove file
    Path(fileName).unlink()    
    
    
    return parser.parse()

def getPrettyTableNews() -> PrettyTable:
    newsList = getSportsNews()
    table = PrettyTable(["Title", "Section", "Source"])
        
    for i in newsList:
        table.add_row([i["title"], i["section"], i["source"]])
        
    return table

def saveNews(path: str) -> True:        
    try:
        with o
            
    except:
        return False

    
    
    
    
    
    