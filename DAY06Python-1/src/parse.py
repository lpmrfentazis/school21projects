# Created by watercre
# Reviewed by vannahyd aka MrFentazis
from bs4 import BeautifulSoup

import json
import yaml



class Parser:
    """
        Class for parse news from sites

        Get news dict - parse(self) -> list[dict]
   
        Save data to file - save(self, path: str) -> bool

    """
    
    def __init__(self, source: str) -> None:
        # Get html page from file to string val
        # encoding="utf-8" for fix russian symbols
        with open(source, "r", encoding="utf-8") as f:
            html= "".join(f.readlines())

        soup = BeautifulSoup(html, "lxml")
        self.data=[]

        news=soup.find_all("a",class_="b-news-list__item-link")
        for item in news:
            topic = {
                        "source": "sport.ru",
                        "section": item.attrs["data-section"],
                        "title": item.text,
                        "link": item.attrs["href"] 
                    }
            self.data.append(topic)
            

    def parse(self) -> list[dict]:
        return self.data


    def save(self, path: str) -> bool:
        """
            if given *.yaml: save yaml
            else: json

            return True if write successful else: False
        """

        try:
            # write yaml
            if "yaml" in path:
                with open(path, 'w', encoding="utf-8") as outfile:
                    yaml.dump(self.data,outfile, encoding="utf-8", allow_unicode=True)

            # json or unexpected
            else:
                with open(path, 'w', encoding="utf-8") as outfile:
                    json.dump(self.data,outfile, ensure_ascii=False)

            return True
        
        except:
            return False


if __name__ == "__main__":
    page = "news.html"
    outfile = "news.yaml"

    parser = Parser(page)
    data = parser.parse()
    parser.save(outfile)
        
